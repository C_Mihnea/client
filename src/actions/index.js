import history from '../history';
import shop from '../apis/shop';

import { 
    SIGN_IN, 
    SIGN_OUT, 
    CREATE_PRODUCT, 
    FETCH_PRODUCT, 
    FETCH_PRODUCTS, 
    EDIT_PRODUCT,
    DELETE_PRODUCT
} from './types';

// auth actions
export const signIn = userId => {
    return {
        type: SIGN_IN,
        payload: userId
    }
}

export const signOut = userId => {
    return {
        type: SIGN_OUT,
    }
}

// product actions
export const createProduct = formValues => async (dispatch, getState) => {
    const { userId } = getState().auth;
    const response = await shop.post('/product', { ...formValues, userId });

    dispatch({ type: CREATE_PRODUCT, payload: response.data })

    history.push('/')
}

export const fetchProducts = () => async dispatch => {
    const response = await shop.get('/product');
    
    dispatch({ type: FETCH_PRODUCTS, payload: response.data.results })
}

export const fetchProduct = (id) => async dispatch => {
    const response = await shop.get(`/product/${id}`)

    dispatch({ type: FETCH_PRODUCT, payload: response.data.result[0] })
}

export const editProduct = (id, formValues) => async dispatch => {
    const response = await shop.put(`product/${id}`)

    dispatch({ type: EDIT_PRODUCT, payload: response.data.result[0] })
} 

export const deleteProduct = (id) => async dispatch => {
    await shop.delete(`/product/${id}`);

    dispatch({ type: DELETE_PRODUCT, payload: id });

    history.push('/');
}