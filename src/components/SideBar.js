import React from 'react';

const SideBar = () => {
    return (
        <div className='ui sidebar inverted vertical menu visible'>
            <a className='logo'>
                <img src="https://react.semantic-ui.com/images/wireframe/image.png" alt='item pic' className='ui tiny image' />
            </a>
            <a className='item'>
                Home
            </a>
            <a className='item'>
                Products
            </a>
            <a className='item'>
                Filter
            </a>
        </div>
    )
}

export default SideBar;