import React from 'react';
import { Link } from 'react-router-dom'

class Item extends React.Component {
       
    render() {

        return (
            <div className='ui segment'>
                <div className='ui middle aligned small image'>
                    <img src="https://react.semantic-ui.com/images/wireframe/image.png" alt='item pic' />
                    <Link to={`/product/${this.props.id}`}>{this.props.name}</Link>
                </div>
            </div>    
        )
    }

}

export default Item;