import React from 'react';
import Item from '../ItemDisplay/Item';
import { connect } from 'react-redux';
import { fetchProduct } from '../../actions';


class productPage extends React.Component {

     componentDidMount() {
    /*    
        // without redux

        //getting the id from the url
        fetch(`http://localhost:4000/product/${this.props.match.params.id}`)
        .then(response => response.json())
        .then(response => this.setState({ product: response.data }))
        .catch(err => console.error(err))
    */

    // with redux
        const { id } = this.props.match.params
        this.props.fetchProduct(id);
    }

    render() {

        const product = { ...this.props.product }
        
        return(
            <div>
                <Item key={product.id} name={product.name} />
                <h1 className='ui'>{product.description}</h1>
                <p>{product.stock}</p>
            </div>    
        )
    }
}

const mapStateToProps = (state, ownProps) => {

    return { product: state.products[ownProps.match.params.id] }
}

export default connect(mapStateToProps, { fetchProduct })(productPage);