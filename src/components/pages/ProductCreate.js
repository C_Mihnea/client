import React from 'react';
import { connect } from 'react-redux';
import { createProduct } from '../../actions';
import ProductForm from './ProductForm';


class ProductCreate extends React.Component {

  // without redux

    constructor() {
      super();
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleSubmit(event) {
      event.preventDefault();
      const product = {
        name: event.target.name.value,
        price: event.target.price.value,
        description: event.target.description.value,
        stock: event.target.stock.value,
        categoryId: event.target.categoryId.value
      }
      
      fetch('http://localhost:4000/product', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          product
        }),
      });
    }
  
    render() {
      return (
        <form onSubmit={this.handleSubmit} method='post' action='http://localhost:4000/product'>
          <label htmlFor="name">Enter name</label>
          <input id="name" name="product[name]" type="text" />
  
          <label htmlFor="price">Enter your price</label>
          <input id="price" name="product[price]" type="text" />

          <label htmlFor="description">Enter your description</label>
          <input id="description" name="product[description]" type="text" />

          <label htmlFor="stock">Enter your stock</label>
          <input id="stock" name="product[stock]" type="text" />

          <label htmlFor="category">Enter your category</label>
          <input id="categoryId" name="product[categoryId]" type="text" />
  
          <button>Submit</button>
        </form>
      );
  }
}

    
/*
  // with redux

    onSubmit = formValues => {
      this.props.createProduct(formValues);
    }

    render() {
      return (
        <div>
          <h3>Create a Product</h3>
          <ProductForm onSubmit={this.onSubmit} />
        </div>
      )
    }
  }
*/

export default ProductCreate