import React from 'react';
import { Field, reduxForm } from 'redux-form';

class ProductForm extends React.Component {

    // render fields error if field is touched and empty
    renderError({ error, touched }) {
        if (error && touched) {
            return (
                <div className='ui error message'>
                    <div className='header'>{error}</div>
                </div>
            );
        }
    }

    // render redux inputs
    renderInput = ({ input, label, meta}) => {

        const className = `field ${meta.error && meta.touched ? 'error': ''}`

        return (
            <div className={className}>
                <label>{label}</label> 
                <input {...input} />
                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = formValues => {
        this.props.onSubmit(formValues)
    }

    render() {
        return (
            <form onSubmit={this.props.handleSubmit(this.onSubmit)} className='ui form error'>
                <Field name='name' component={this.renderInput} label='Enter name' />
                <Field name='price' component={this.renderInput} label='Enter price' />
                <Field name='description' component={this.renderInput} label='Enter description' />
                <Field name='stock' component={this.renderInput} label='Enter stock' />
                <Field name='categoryId' component={this.renderInput} label='Enter category' />
                <button className='ui button primary'>Submit</button>
            </form>
        )
    }
}

const validate = formValues => {
    const errors = {}

    if (!formValues.name) {
        errors.name = 'You must enter a name!'
    }
    if (!formValues.price) {
        errors.price = 'You must enter a price!'
    }
    
    return errors
}

export default reduxForm({
    form: 'productForm',
    validate
})(ProductForm);