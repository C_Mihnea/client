import React from 'react'
import Item from '../ItemDisplay/Item';
import { fetchProducts } from '../../actions/';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class home extends React.Component {

    componentDidMount() {
        // without redux
        /*
        fetch('http://localhost:4000/product')
            .then(response => response.json())
            .then(response => this.setState({ products: response.data }))
            .catch(err => console.error(err))
        */

        // with redux
        this.props.fetchProducts();
    }

    renderAdmin(product) {
        
            return (
                <div className='right floated content'>
                    <Link to={`/product/${product.id}`} className='ui button primary'>
                        Edit
                    </Link>
                    <Link to={`/product/delete/${product.id}`} className='ui button negative'>
                        Delete
                    </Link>
                </div>
            )
        
    }

    renderCreate() {
        if (this.props.isSignedIn) {
            return (
                <div style={{ textAlign: 'right' }}>
                    <Link to='/create-product/' className='ui button primary'>
                        New Product
                    </Link>
                </div>
            )
        }
    }

    renderList() {
       return this.props.products.map(product => {
           return (
            <div key={product.id}>
                <Item id={product.id} name={product.name}  />
                <div>
                    {this.renderAdmin(product)}
                </div>
            </div>
           )
       })
    }

    render() {
        //const { products } = this.state;

        return (
            <div>
                <div className="App">
                    {this.renderList()}
                    {this.renderCreate()}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    
    return {
        products: Object.values(state.products),
        currentUserId: state.auth.userId,
        isSignedIn: state.auth.isSignedIn
    }
}

export default connect(mapStateToProps, { fetchProducts })(home);