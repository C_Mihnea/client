import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from '../history';

//components
import Header from './Header';
//import SideBar from './SideBar';

//pages
import home from './pages/home';
import ProductPage from './pages/ProductPage';
import ProductCreate from'./pages/ProductCreate';
import login from '../components/pages/login';
import ProductForm from '../components/pages/ProductForm'
import ProductDelete from './pages/ProductDelete';

const App = () => {
        
    return (
        <div>
            <Router history={history}>
                <Header />
                <Switch>
                    <Route path='/' exact component={home} />
                    <Route path='/product/:id' exact component={ProductPage} />
                    <Route path='/create-product/' exact component={ProductCreate} />
                    <Route path='/product/delete/:id' exact component={ProductDelete} />
                    <Route path='/register' exact component={login} />
                    <Route path='/test' exact component={ProductForm} />
                </Switch>
            </Router> 
        </div>
    )
}

export default App;